<?php
namespace SigningClient\Models;

/**
 * Class Document
 * @package signingClient\models
 *
 * @method getId()
 * @method getTitle()
 * @method getDownloadPath()
 * @method getPages()
 * @method getAuthorID()
 * @method getStatus()
 *
 */
class Document extends BaseModel
{
    protected $id;
    protected $title;
    protected $downloadPath;
    protected $pages;
    protected $authorID;

    public function __construct($result)
    {
        $this->id = $result["id"];
        $this->title = $result["title"];
        $this->downloadPath = $result["path"];
        $this->pages = $result["pages"];
        $this->authorID = $result["authorID"];
    }
}