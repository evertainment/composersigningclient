<?php
namespace SigningClient\Models;

/**
 * Class Field
 * @package signingClient\models
 *
 * @method getType()
 * @method getPage()
 * @method getRectangle()
 * @method getRequired()
 *
 */
class Field extends BaseModel
{
	const TYPE_INPUT = 'input';
	const TYPE_DATE = 'date';
	const TYPE_SIGNATURE = 'signature';
	const TYPE_CHECK_BOX = 'checkbox';

    protected $baseWidth = 1000;

    protected $title;
    protected $type;
    protected $rectangle;
    protected $page;
    protected $required = false;

    /**
     * Field constructor.
     * @param $type
     * @param $page
     * @param Rectangle $rectangle
     * @param bool|false $required
     */
    public function __construct($type, $page, Rectangle $rectangle, $title = "", $required = false)
    {
        $this->type = $type;
        $this->title = $title;
        $this->page = $page;
        $this->rectangle = $rectangle->asArray();
        $this->required = $required;
    }

    /**
     * @param $value
     * @return $this
     */
    public function changeBaseWidth($value)
    {
        $this->baseWidth = $value;
        return $this;
    }
}