<?php
namespace SigningClient\Models;

/**
 * Class Rectangle
 * @package SigningClient\Models
 */
class Rectangle
{
    protected $width;
    protected $height;
    protected $x;
    protected $y;

    /**
     * Rectangle constructor.
     * @param $width
     * @param $height
     * @param $top
     * @param $left
     */
    public function __construct($x, $y, $width, $height)
    {
        $this->width = $width;
        $this->height = $height;
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * @return array
     */
    public function asArray()
    {
        return [
            $this->x,
            $this->y,
            $this->width,
            $this->height
        ];
    }
}