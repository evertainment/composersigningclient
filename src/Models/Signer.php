<?php
namespace SigningClient\Models;

/**
 * Class Signer
 * @package signingClient\models
 *
 * @method getName()
 * @method getEmail()
 * @method getFields()
 *
 */
class Signer extends BaseModel
{
    protected $email;
    protected $name;
    protected $fields = [];

    public function __construct($name, $email)
    {
        $this->name = $name;
        $this->email = $email;
    }

    public function addField(Field $field)
    {
        $this->fields[] = $field->getObjectVars();
    }
}