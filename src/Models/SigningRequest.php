<?php
namespace signingClient;

class SignatureRequest
{
    /**
     * @var string
     */
    protected $file;

    public function __construct($file)
    {
        $this->file = $file;
    }
}