<?php
namespace SigningClient\Models;

/**
 * Class SignatureRequest
 * @package SigningClient\Models
 *
 * @method Document getDocument()
 * @method array getSigners()
 * @method string getId()
 * @method SignatureRequest setId($value)
 * @method SignatureRequest setDocument($value)
 * @method SignatureRequest setLogoUrl($value)
 * @method SignatureRequest setIntroText($value)
 * @method SignatureRequest setCompleted($value)
 * @method SignatureRequest setRedirectAfterSigning($value)
 * @method string getLogoUrl()
 * @method string getIntroText()
 * @method string getRedirectAfterSigning()
 */
class SignatureRequest extends BaseModel
{
    protected $id;

    /**
     * @var array
     */
    protected $signers = [];

    protected $document;

    protected $introText;

    protected $logoUrl;

    protected $redirectAfterSigning;

    protected $completed;

    protected function __construct()
    {
    }

    /**
     * @param array $apiData
     * @return SignatureRequest
     */
    public static function createByApi($apiData)
    {
        $request = new SignatureRequest();
        $request->setId($apiData['id']);
        $request->setLogoUrl($apiData['logoUrl']);
        $request->setRedirectAfterSigning($apiData['redirectAfterSigning']);
        $request->setIntroText($apiData['introText']);
        $request->setCompleted($apiData['completed']);
        return $request;
    }

    /**
     * @param Document $document
     * @return SignatureRequest
     */
    public static function createByDocument(Document $document)
    {
        $request = new SignatureRequest();
        $request->setDocument($document);
        return $request;
    }

    /**
     * @param Signer $signer
     */
    public function addSigner(Signer $signer)
    {
        $this->signers[] = $signer->getObjectVars();
    }

    /**
     * @return mixed
     */
    public function isCompleted()
    {
        return $this->completed;
    }
}