<?php
namespace SigningClient\Models;

class BaseModel
{
    public function __call($method, $parameters)
    {
        if(substr($method,0,3) == "get")
        {
            $var = substr($method, 3, strlen($method)-3);
            $var = lcfirst($var);
            if(property_exists($this, $var))
            {
                $val = $this->$var;
                return $val;
            }
            throw new \Exception("Field " . $var . " does not exist");
        }
        elseif(substr($method,0,3) == "set")
        {
            $var = substr($method, 3, strlen($method)-3);
            $var = lcfirst($var);
            if(property_exists($this, $var))
            {
                $this->$var = $parameters[0];
            }
            else
            {
                throw new \Exception("Field " . $var . " does not exist");
            }
            return $this;
        }
    }

    /**
     * @return array
     */
    public function getObjectVars()
    {
        return get_object_vars($this);
    }
}