<?php
namespace signingClient;

class Result
{
    /**
     * @var int
     */
    protected $error = 500;

    /**
     * @var string
     */
    protected $message = "failed request";

    /**
     * @var mixed
     */
    protected $result = null;

    /**
     * Result constructor.
     * @param string $response
     */
    public function __construct($response)
    {
        if(!is_null($response))
        {
            $response = json_decode($response, true);
            $this->error = $response['error'];
            $this->message = $response['message'];
            $this->result = $response['result'];
        }
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        if($this->error != 0)
        {
            return false;
        }
        return true;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return int
     */
    public function getError()
    {
        return $this->error;
    }
}