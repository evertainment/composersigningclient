<?php
namespace SigningClient;

use GuzzleHttp\Client;
use SigningClient\Models\Document;
use SigningClient\Models\SignatureRequest;

class SigningClient
{
    protected $oauthToken = null;
    protected $url = "https://signing.evertainment.com/api/v1/";

    public function __construct($customerToken, $secret)
    {
        $result = $this->token($customerToken, $secret);

        if($result->isSuccess())
        {
            $result = $result->getResult();
            $this->oauthToken = $result['token'];
        }
    }

    /**
     * @return array
     */
    private function getHeaders()
    {
        $headers = [];
        if(!is_null($this->oauthToken))
        {
            $headers['OAuthToken'] = $this->oauthToken;
        }
        return $headers;
    }

    /**
     * @param string $method
     * @param string $endpoint
     * @param array $data
     * @return Result
     */
    private function send($method = 'get', $endpoint = "", $data = [])
    {
        $client = new Client([
            'http_errors' => false
        ]);
        $response = null;
        try
        {
            if($method == 'get' && count($data) > 0)
            {
                $getParams = [];
                foreach($data as $var=>$value)
                {
                    $getParams[] = $var."=".$value;
                }
                $endpoint = $endpoint."?".implode("&", $getParams);
                $data = [];
            }
            $params = [
                'headers'=>$this->getHeaders()
            ];
            if(count($data) > 0)
            {
               $params['body'] = json_encode($data);
            }
            $response = $client->request($method, $this->url.$endpoint, $params);
            $response = $response->getBody()->getContents();
            $response = $this->result($response);
        }
        catch(\GuzzleHttp\Exception\GuzzleException $ex)
        {
            print_r($ex);
            #$response = $response->getBody()->getContents();
        }
        return $response;
    }

    /**
     * @param $customerToken
     * @param $secret
     * @return Result
     */
    private function token($customerToken, $secret)
    {
        return $this->send("get", "token/".$customerToken."/".$secret, []);
    }

    /**
     * @param $response
     * @return Result|SignatureRequest
     */
    private function result($response)
    {
        return new Result($response);
    }

    public function sendSignatureRequest(SignatureRequest $signatureRequest)
    {
        $document = $signatureRequest->getDocument();

        $data = [
            'Signers' => $signatureRequest->getSigners(),
            'IntroText' => $signatureRequest->getIntroText(),
            'LogoUrl' => $signatureRequest->getLogoUrl(),
            'RedirectAfterSigning' => $signatureRequest->getRedirectAfterSigning()
        ];
        $response = $this->send("post", "document/signatureRequest/".$document->getId(), $data);

        if($response->isSuccess())
        {
            $result = $response->getResult();
            $signatureRequest->setId($result['id']);
            return $signatureRequest;
        }
        else
        {
            return $response->getError();
        }
    }

    /**
     * @param $pathToPdf
     * @param string $title
     * @param string $authorID
     * @param null $filename
     * @return null|Document
     */
    public function uploadDocument($pathToPdf, $title, $authorID, $filename = null)
    {
        if(is_null($filename))
        {
            $filename = pathinfo($pathToPdf)["basename"];
        }

        $response = $this->send("post", "document/upload",[
            'File' => base64_encode(file_get_contents($pathToPdf)),
            'AuthorID' => $authorID,
            'Title' => $title,
            'Filename' => $filename
        ]);

        if($response->isSuccess())
        {
            return new Document($response->getResult());
        }
        else
        {
            return null;
        }
    }

    /**
     * @param $documentID
     * @return Document
     */
    public function document($documentID)
    {
        if($documentID instanceof Document)
        {
            $documentID = $documentID->getId();
        }
        $response = $this->send("get", "document/".$documentID);
        if($response->isSuccess())
        {
            return new Document($response->getResult());
        }
        else
        {
            return null;
        }
    }

    /**
     * @param $signatureRequestID
     * @return string
     */
    public function signedDocument($signatureRequestID)
    {
        if($signatureRequestID instanceof SignatureRequest)
        {
            $signatureRequestID = $signatureRequestID->getId();
        }
        $response = $this->send("get", "signatureRequest/signedDocument/".$signatureRequestID);
        if($response->isSuccess())
        {
            return $response->getResult();
        }
        else
        {
            return null;
        }
    }

    /**
     * @param $signatureRequestID
     * @return Document
     */
    public function signatureRequest($signatureRequestID)
    {
        if($signatureRequestID instanceof SignatureRequest)
        {
            $signatureRequestID = $signatureRequestID->getId();
        }
        $response = $this->send("get", "signatureRequest/".$signatureRequestID);
        if($response->isSuccess())
        {
            return SignatureRequest::createByApi($response->getResult());
        }
        else
        {
            return null;
        }
    }

    /**
     * @param $signatureRequestID
     * @return null|SignatureRequest
     */
    public function signerUrls($signatureRequestID)
    {
        if($signatureRequestID instanceof SignatureRequest)
        {
            $signatureRequestID = $signatureRequestID->getId();
        }
        $response = $this->send("get", "signatureRequest/signerUrls/".$signatureRequestID);
        if($response->isSuccess())
        {
            return $response->getResult();
        }
        else
        {
            return null;
        }

    }
}